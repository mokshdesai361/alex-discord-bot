exports.run = (client, message, args) => {
    const Discord = require('discord.js')
    const tdm = new Discord.MessageEmbed()
        .setTitle("Game Mode : Team Deathmatch")
        .setColor("RANDOM")
        .setDescription("Defeat players on the opposing team.")
        .setTimestamp()
    return message.channel.send({embeds: [tdm]});   
}