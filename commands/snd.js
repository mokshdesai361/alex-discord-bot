exports.run = (client, message, args) => {
    const Discord = require('discord.js')
    const snd = new Discord.MessageEmbed()
        .setTitle("Game Mode: Search And Destroy")
        .setColor("RANDOM")
        .setDescription("Round Based mode. Destroy one of tow objectives site on the map using a c4 or kill all enemy.")
        .setTimestamp()
    return message.channel.send({embeds: [snd]});
}