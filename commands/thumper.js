const guns = require('../data/guns.json')
const Discord = require("discord.js");
exports.run = (client, message, args) => {
    const Discord = require('discord.js')
    const thumper = guns.find (gun => gun.name === "thumper")
    const thumperr = new Discord.MessageEmbed()
        .setTitle("Thumper Base Stats")
        .setColor("RANDOM")
        .setImage('https://i.imgur.com/RxdAydR.jpeg')
        .setDescription(`Thumper has base damage of ${thumper.baseDamage}, \n Thumper has base Accuracy of ${thumper.baseAccuracy}, \n Thumper has base Firerate of ${thumper.baseFirerate}`)
    return message.channel.send({embeds: [thumperr]})
}